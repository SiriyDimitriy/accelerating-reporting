import {deepCopy} from '../utils/dataUtils';

const initialState = {
    entities: null,
    // selectedEntities: [],
    pageSize: localStorage.getItem('itemsOnPage') ? JSON.parse(localStorage.getItem('itemsOnPage')) : 100,
    pageNumber: 0,
    searchIsDisabled: false,
    // sortBy: 'name',
    // sortOrder: 'asc',
};

const searchResultsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_ENTITIES': {
            const newState = deepCopy(state);

            newState.entities = action.entities;

            return newState;
        }

        case 'UPDATE_SELECTED_ENTITIES': {
            const newState = deepCopy(state);

            newState.selctedEntities = action.entities;

            return newState;
        }

        case 'SET_PAGE_SIZE': {
            const newState = deepCopy(state);

            newState.pageSize = action.pageSize;

            return newState;
        }

        case 'SET_PAGE_NUMBER': {
            const newState = deepCopy(state);

            newState.pageNumber = action.pageNumber;

            return newState;
        }

        default:
            return state;
    }
};

const SearchResultsReducer = {
    searchResults: searchResultsReducer,
};

export default SearchResultsReducer;
