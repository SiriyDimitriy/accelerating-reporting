import {deepCopy} from '../utils/dataUtils';

const initialState = {
    showLoader: false,
};

const loaderReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'App/TOGGLE_LOADER':
            const newState = deepCopy(state);

            newState.showLoader = action.showLoader;

            return newState;

        default:
            return state;
    }
};

const LoaderReducer = {
    loader: loaderReducer,
};

export default LoaderReducer;
