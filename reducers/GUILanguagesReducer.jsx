import {deepCopy} from '../utils/dataUtils';

const initialState = {
    GUILanguages: [],
    GUILanguagesErrors: [],
};

const UILanguagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_GUI_LANGUAGES_LIST':
            const newState = deepCopy(state);

            newState.GUILanguages = action.lngs;

            return newState;

        default:
            return state;
    }
};

const GUILanguagesErrorsReducer = (state = null, action) => {
    switch (action.type) {
        case 'GET_GUI_LANGUAGES_LIST_FAILED':
            const newState = deepCopy(state);

            newState.GUILanguagesErrors = action.err;

            return newState;

        default:
            return state;
    }
};

const GUILanguagesReducer = {
    GUILanguages: UILanguagesReducer,
    GUILanguagesErrors: GUILanguagesErrorsReducer,
};

export default GUILanguagesReducer;
