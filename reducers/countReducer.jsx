const initialState = {
        count: null,
};

const countReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_ITEMS_COUNT':

            const newState = { ...state };

            newState.count = action.count;

            return newState;

        default:

            return state;
    }
};

const CountReducer = {
    count: countReducer,
};

export default CountReducer;