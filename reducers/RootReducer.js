import {combineReducers} from 'redux';
import GUILanguagesReducer from './GUILanguagesReducer';
import LoaderReducer from './loaderReducer';
import SearchControlsReducer from './searchControlsReducer';
import SearchResultsReducer from './searchResultsReducer';
import CountReducer from './countReducer';

const RootReducer = combineReducers(
    Object.assign({},
        GUILanguagesReducer,
        LoaderReducer,
        SearchControlsReducer,
        SearchResultsReducer,
        CountReducer,
    )
);

export default RootReducer;
