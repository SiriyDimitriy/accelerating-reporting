const initialState = {
    dateFrom: null,
    dateTo: null,
    userString: null,
};

const searchControlsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_DATE_FROM': {
            const newState = {...state};

            newState.dateFrom = action.date;

            return newState;
        }

        case 'UPDATE_DATE_TO': {
            const newState = {...state};

            newState.dateTo = action.date;

            return newState;
        }

        case 'UPDATE_USER_STRING': {
            const newState = {...state};

            newState.userString = action.string;

            return newState;
        }

        default:
            return state;
    }
};

const SearchControlsReducer = {
    searchControls: searchControlsReducer,
};

export default SearchControlsReducer;
