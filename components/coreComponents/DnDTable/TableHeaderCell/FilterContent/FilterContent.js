import React from 'react';
import Select from '../../../../simple/Select/Select';
import ActionButton from '../../../../simple/Button/Button';
import FilterContentStyles from './FilterContent.less';
import { getTranslation, getTranslationWithParams } from '../../../../../utils/translationsUtils';
import Loader from '../../../../simple/Loader/Loader';

export default class FilterContent extends React.PureComponent {
    constructor(props) {
        super(props);
        const { searchOptions, columnId, filter: { type } } = this.props;
        let currentFilter = searchOptions.searchOptions.projectionFilters.find(el => el.columnId === columnId);
        this.state = {
            selectItems: [],
            textFilter: currentFilter && type === 'string' ? currentFilter.value : null,
            selectFilterSelectedValue: currentFilter && type === 'select' ? currentFilter.value : null,
            isFilterExist: searchOptions.searchOptions.projectionFilters.find(el => el.columnId === columnId),
        };
        this.onSelectHandler = this.onSelectHandler.bind(this);
        this.onSubmitHandler = this.onSubmitHandler.bind(this);
        this.onClearHandler = this.onClearHandler.bind(this);
        this.onTextFieldChangeHandler = this.onTextFieldChangeHandler.bind(this);
    }

    UNSAFE_componentWillMount() {
        const { filter } = this.props;

        if (filter && filter.selectItems && filter.selectItems.length !== 0) {
            const messageKeys = [];
            let selectOptions = filter.selectItems.map(item => {
                if (item.labelToken && item.labelToken.messageKey) {
                    messageKeys.push(item.labelToken.messageKey);
                }

                if (item.labelToken && item.labelToken.text) {
                    item.label = item.labelToken.text;
                }
                return item;
            });
            if (messageKeys.length !== 0) {
                getTranslation(messageKeys).then(res => {
                    selectOptions.forEach(el => {
                        if (el.labelToken) {
                            el.label = getTranslationWithParams(res[el.labelToken.messageKey], el.labelToken.params);
                        }
                    });
                    this.setState({ selectItems: selectOptions });
                });
            } else {
                this.setState({ selectItems: selectOptions });
            }
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const { searchOptions, columnId } = this.props;

        if (nextProps.searchOptions.searchOptions.projectionFilters !== searchOptions.searchOptions.projectionFilters
            || nextProps.columnId !== columnId) {
            this.setState({ isFilterExist: nextProps.searchOptions.searchOptions.projectionFilters.find(el => el.columnId === nextProps.columnId) });
        }
    }

    onSelectHandler(val) {
        const { columnId, searchOptions, filter: { type } } = this.props;
        let projectionFilters = searchOptions.searchOptions.projectionFilters;

        this.setState({ selectFilterSelectedValue: val });

        if (type === 'select') {
            if (val) {
                let currentFilter = projectionFilters.find(el => el.columnId === columnId);
                if (currentFilter) {
                    currentFilter.value = val;
                } else {
                    projectionFilters.push({
                        'columnId': columnId,
                        'value': val,
                    });
                }
            } else {
                searchOptions.searchOptions.projectionFilters = projectionFilters.filter(filter => filter.columnId !== columnId);
                this.setState({ selectFilterSelectedValue: null });
            }
        }

        searchOptions.firstResult = 0;
    }

    onSubmitHandler() {
        const { handleChangeSearchOptions, searchOptions, columnId, filter: { type }, hideTooltip } = this.props;
        let projectionFilters = searchOptions.searchOptions.projectionFilters;
        const { textFilter } = this.state;

        if (textFilter !== '') {
            if (type === 'string') {
                let currentFilter = searchOptions.searchOptions.projectionFilters.find(el => el.columnId === columnId);
                if (currentFilter) {
                    currentFilter.value = textFilter;
                } else {
                    searchOptions.searchOptions.projectionFilters.push({
                        'columnId': columnId,
                        'value': textFilter.toString(), //for type string
                    });
                }
            }
        } else {
            searchOptions.searchOptions.projectionFilters = projectionFilters.filter(filter => filter.columnId !== columnId);
            this.setState({ textFilter: null });
        }

        searchOptions.firstResult = 0;

        handleChangeSearchOptions(searchOptions);
        hideTooltip();
    }

    onClearHandler() {
        const { handleChangeSearchOptions, searchOptions, columnId, hideTooltip } = this.props;
        let projectionFilters = searchOptions.searchOptions.projectionFilters;

        searchOptions.searchOptions.projectionFilters = projectionFilters.filter(filter => filter.columnId !== columnId);
        handleChangeSearchOptions(searchOptions);
        hideTooltip();
    }

    onTextFieldChangeHandler(e) {
        this.setState({ textFilter: e.target.value });
    }

    render() {
        const { filter: { type }, filterTranslations } = this.props;
        const { textFilter, isFilterExist, selectFilterSelectedValue, selectItems } = this.state;
        const clearBtnText = filterTranslations && filterTranslations.clearBtnText || 'Clear';

        return ( //input wrapper was added for CommonStyles applying, input styling is not possible (old app influence)
            <div>
                {type === 'string' && <React.Fragment>
                    <div className={FilterContentStyles.text_field_wrapper}>
                        <input type={'text'} onChange={this.onTextFieldChangeHandler} value={textFilter}
                               className={FilterContentStyles.text_field}/>
                    </div>
                    <div className={FilterContentStyles.controls_wrapper}>
                        <ActionButton onClick={this.onSubmitHandler} text={'OK'} type={'default'}/>
                        {isFilterExist &&
                        <ActionButton onClick={this.onClearHandler} text={clearBtnText} type={'default'}/>}
                    </div>
                </React.Fragment>}
                {type === 'select' && selectItems.length !== 0 && <React.Fragment>
                    <Select options={selectItems}
                            defaultValue={selectFilterSelectedValue}
                            onSelectHandler={this.onSelectHandler}
                    />
                    <div className={FilterContentStyles.controls_wrapper}>
                        {isFilterExist &&
                        <ActionButton onClick={this.onClearHandler} text={clearBtnText} type={'default'}/>}
                        <ActionButton onClick={this.onSubmitHandler} text={'OK'} type={'default'}/>
                    </div>
                </React.Fragment>}
                {type === 'select' && selectItems.length === 0 &&
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Loader color={'#777'} dotSize={'8px'} dotsCount={12} radius={'50px'}/>
                </div>
                }
            </div>
        );
    }
}
