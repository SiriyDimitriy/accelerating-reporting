import React from 'react';
import ReactTooltip from 'react-tooltip';
import TableHeaderCellStyles from './TableHeaderCell.less';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import TableBodyCell from '../TableBodyCell/TableBodyCell';
import CustomTooltip from '../../../simple/Tooltip/Tooltip';
import FilterContent from './FilterContent/FilterContent';
import CustomReactTooltip from '../../../simple/CustomReactTooltip/CustomReactTooltip';
import FilterIconComponent from './FilterIconComponent/FilterIconComponent';
import Button from '../../../simple/Button/Button';

let cx = classNames.bind(TableHeaderCellStyles);

class TableHeaderCell extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            isEllipsisActive: false,
            // props.searchOptions is not present in the table of selected items
            isFilterActive: props.searchOptions && props.searchOptions.searchOptions.projectionFilters.find(el => el.columnId === props.columnId),
            headerClass: cx({
                'header_cell': true,
                [props.colIndex]: true,
                'with_actions_1': true,
            }),
        };
    }

    componentDidMount() {
        const { colWidth, text, searchOptions, columnId } = this.props;
        const cellWidth = this.cell && (this.cell.scrollWidth || this.cell.offsetWidth);
        if (text && searchOptions) {
            this.setState(() => ({
                isEllipsisActive: colWidth < cellWidth,
                isFilterActive: searchOptions.searchOptions.projectionFilters
                    .find(el => el.columnId === columnId),
            }));
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const { columnId, colWidth } = this.props;
        if (this.cell && nextProps.colWidth && nextProps.text && (nextProps.colWidth !== colWidth)) {
            const cellWidth = this.cell.scrollWidth || this.cell.offsetWidth;
            this.setState(() => ({ isEllipsisActive: nextProps.colWidth < cellWidth }));
        }
        if (nextProps.searchOptions && nextProps.searchOptions.searchOptions && nextProps.searchOptions.searchOptions.projectionFilters) {
            this.setState(() => ({
                isFilterActive: nextProps.searchOptions.searchOptions.projectionFilters
                    .find(el => el.columnId === columnId),
            }));
        }
    }

    changeSortingDirection = () => {
        const { hasSorting, onHeaderClickHandler, columnId, isDataEmpty } = this.props;

        if (hasSorting && !isDataEmpty) {
            onHeaderClickHandler(columnId);
        }
    };

    hideTooltipViaTimeout = () => {
        setTimeout( () => ReactTooltip.hide(this.cell), 1000);
    };

    render() {
        const {
            filter, hasSorting, resizable, searchOptions, handleChangeSearchOptions,
            columnId, languageFallback, filtering, sorting, text, filterTranslations
        } = this.props;
        const { isEllipsisActive, isFilterActive, headerClass } = this.state;

        return (
            <th className={headerClass}>
                {text && (filter || hasSorting) ?
                    <div className={TableHeaderCellStyles.table_head_actions}
                         data-cell='remove-before-convert-to-excel'>
                        {filtering && filter &&
                        (filter.selectItems && filter.selectItems.length === 0 ? null :
                            <CustomTooltip target={<FilterIconComponent isFilterActive={isFilterActive}/>}
                                           position={'bottom'}
                                           ref={tooltip => this.tooltip = tooltip}
                                           id={'tooltip_' + columnId}
                                           portalRendering
                            >
                                <FilterContent filter={filter}
                                               searchOptions={searchOptions}
                                               handleChangeSearchOptions={handleChangeSearchOptions}
                                               columnId={columnId}
                                               filterTranslations={filterTranslations}
                                />
                            </CustomTooltip>)}
                        {sorting && searchOptions && searchOptions.sortBy === columnId && searchOptions.sortOrder === 'asc'
                        && <Button type={'icon'} iconUrl={'/img/up-arrow.png'} className={TableHeaderCellStyles.arrow_icon}
                                        onClick={this.changeSortingDirection}/>}
                        {sorting && searchOptions && searchOptions.sortBy === columnId && searchOptions.sortOrder === 'desc'
                        && <Button type={'icon'} iconUrl={'/img/down-arrow.png'} className={TableHeaderCellStyles.arrow_icon}
                                          onClick={this.changeSortingDirection}/>}
                    </div> : null
                }
                {text && <div className={TableHeaderCellStyles.header_cell_content}>
                    {resizable && <div className={TableHeaderCellStyles.grip} data-element='resizable'>&nbsp;</div>}
                    <span data-tip=''
                          data-for={text.toString()}
                          className={TableHeaderCellStyles.data_container}
                          ref={(cell) => this.cell = cell}
                          style={hasSorting ? { cursor: 'pointer' } : {}}
                          onClick={this.changeSortingDirection}
                    >
                        {languageFallback && <img className={TableHeaderCellStyles.warning_icon}
                                                  src={require('../assets/warning.gif')}
                                                  data-cell='remove-before-convert-to-excel'
                                                  alt='Not translated'/>}
                        {text}
                    </span>
                    {!hasSorting && <div data-cell='remove-before-convert-to-excel'>
                        <ReactTooltip id={text.toString()}
                                      data-cell='remove-before-convert-to-excel'
                                      place='top'
                                      type='light'
                                      border={true}
                                      event={'click'}
                                      globalEventOff='click'
                                      effect='solid'
                                      afterShow={this.hideTooltipViaTimeout}>
                            <div style={{ pointerEvents: 'auto' }} onClick={(e) => e.stopPropagation()}>
                                No sorting possible
                            </div>
                        </ReactTooltip>
                    </div>}
                    {isEllipsisActive && <div data-cell='remove-before-convert-to-excel'>
                        <CustomReactTooltip tooltipId={text.toString()} place={'bottom'} offset={{ top: 2 }}>
                            {text}
                        </CustomReactTooltip>
                    </div>
                    }
                </div>}
            </th>
        );
    }
}

export default TableHeaderCell;

TableBodyCell.propTypes = {
    className: PropTypes.string,
    colWidth: PropTypes.number,
};
