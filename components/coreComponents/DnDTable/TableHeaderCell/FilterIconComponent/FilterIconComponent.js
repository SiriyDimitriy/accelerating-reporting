import React from 'react';
import FilterIconComponentStyles from './FilterIconComponent.less';
import Button from '../../../../simple/Button/Button';

const FilterIconComponent = ({ isFilterActive }) => {
    return (
        isFilterActive
            ? <Button type={'icon'} iconUrl={'/img/filter-filled.png'} className={FilterIconComponentStyles.filter_icon}
                                data-tip data-for={'filterIcon'}/>
            : <Button type={'icon'} iconUrl={'/img/filter-empty.png'} className={FilterIconComponentStyles.filter_icon}
                          data-tip data-for={'filterIcon'}/>
    );
};

export default FilterIconComponent;
