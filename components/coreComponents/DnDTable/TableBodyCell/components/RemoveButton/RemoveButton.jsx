import React from 'react';
import Styles from './RemoveButton.less';

const appContext = process.env.APPLICATION_CONTEXT;

const RemoveButton = ({ onClickHandler }) => <img src={`/${appContext}/img/delete.png`}
                                                  alt={'remove value'}
                                                  onClick={onClickHandler}
                                                  className={Styles.remove_button}/>;

export {
    RemoveButton,
};