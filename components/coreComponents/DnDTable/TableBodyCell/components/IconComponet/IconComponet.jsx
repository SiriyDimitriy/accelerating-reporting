import React from 'react';
import Styles from './IconComponet.less';

const IconComponet = ({ id, isChangeRequest }) => <img src={'/pcpm/img/delete.png'}
                                                       alt={'remove value'}
                                                       onClick={() => this.onDeleteHandler(id, isChangeRequest)}
                                                       className={Styles.remove_button}/>;

export {
    IconComponet,
};