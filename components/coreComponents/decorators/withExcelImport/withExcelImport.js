/*eslint-disable */
import React from 'react';
import ExcelImportStyles from './withExcelImport.less';
import {isFirefox, isMsie} from '../../../../utils/navigatorUtils';
import ExcelIcon from '../../../../assets/svg/excel_icon.svg';

export default class ExcelImport extends React.PureComponent {

    wrappedTable = React.createRef();

    constructor(props) {
        super(props);
        this._fallbacktoCSV = true;

        this.exportToXLS = this.exportToXLS.bind(this, props.filename);
        this.exportToCSV = this.exportToCSV.bind(this, props.filename);
    }

    exportToXLS(filename) {
        this._filename = filename;

        //Fallback to CSV for IE & Edge
        if ((isMsie() || isFirefox()) && this._fallbacktoCSV) {
            return this.exportToCSV('exportTable');
        }

        let uri = 'data:application/vnd.ms-excel;base64,'
            ,
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>' // eslint-disable-line max-len
            , base64 = (s) => window.btoa(unescape(encodeURIComponent(s)))
            , format = (s, c) => s.replace(/{(\w+)}/g, (m, p) => c[p]);

        const createLink = () => {
            console.log('this.wrappedTable.current', this.table);
            //remove cells with checkbox
            const tableClone = this.table.cloneNode(true);
            console.log('tableClone', tableClone);
            tableClone.querySelectorAll('[data-cell=remove-before-convert-to-excel]')
                .forEach((e) => e.parentNode.removeChild(e));
            let ctx = {worksheet: name || 'Worksheet', table: tableClone.innerHTML};
            console.log('tableClone removed elements', tableClone);
            console.log('tableClone.innerHTML', tableClone.innerHTML);

            let anchor = document.createElement('a');
            anchor.style = 'display:none !important';
            anchor.id = 'downloadanchor';
            document.body.appendChild(anchor);

            // If the [download] attribute is supported, try to use it
            if ('download' in anchor) {
                anchor.download = this._filename + '.' + 'xls';
            }
            anchor.href = uri + base64(format(template, ctx));

            console.log('base64(format(template, ctx))', base64(format(template, ctx)));
            console.log('this._filename', this._filename);
            console.log('anchor.href', anchor.href);
            console.log('anchor.download', anchor.download);
            console.log('anchor', anchor);
            anchor.click();
        };

        createLink();
    }

    exportToCSV(filename) {
        this._filename = filename;
        let table = this.wrappedTable.current.cloneNode(true);
        // Remove from table headers div with resize-handler
        let headerWithResize = table.querySelectorAll('th > div');
        Array.prototype.slice.call(headerWithResize, 0) // need for IE11: Object doesn't support property or method 'forEach'
            .forEach((e) => e.parentNode.removeChild(e));
        //remove cells with checkbox
        let cellsWithCheckbox = table.querySelectorAll('[data-cell=remove-before-convert-to-excel]');
        Array.prototype.slice.call(cellsWithCheckbox, 0) // need for IE11: Object doesn't support property or method 'forEach'
            .forEach((e) => e.parentNode.removeChild(e));
        // Generate our CSV string from out HTML Table
        let csv = this._tableToCSV(table);
        // Create a CSV Blob
        let blob = new Blob([csv], {type: 'text/csv;charset=utf-8;'});

        // Determine which approach to take for the download
        if (navigator.msSaveOrOpenBlob) {
            // Works for Internet Explorer and Microsoft Edge
            navigator.msSaveOrOpenBlob(blob, this._filename + '.csv');
        } else {
            this._downloadAnchor(URL.createObjectURL(blob), 'csv');
        }
    }

    _downloadAnchor(content, ext) {
        let anchor = document.createElement('a');
        anchor.style = 'display:none !important';
        anchor.id = 'downloadanchor';
        document.body.appendChild(anchor);

        // If the [download] attribute is supported, try to use it
        if ('download' in anchor) {
            anchor.download = this._filename + '.' + ext;
        }
        anchor.href = content;
        anchor.click();
        anchor.remove();
    }

    _tableToCSV(table) {
        // We'll be co-opting `slice` to create arrays
        let slice = Array.prototype.slice;

        return slice
            .call(table.rows)
            .map(row => slice.call(row.cells)
                .map(cell => '"t"'.replace('t', cell.textContent)).join(',')).join('\r\n');
    }

    render() {
        const {columns, data} = this.props;

        return (// icon wrapped by span for title attribute
            <React.Fragment>
                <table ref={(el) => this.table = el} style={{display: 'none'}}>
                    <thead>
                    <tr>
                        {columns && columns.map(col => <th key={col.id}>{col.label}</th>)}
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((row, i) => {
                        let rowStyle = {
                            backgroundColor: (i % 2 === 0 && '#F0F0F0'), //stripped bg-color for row
                        };

                        return ( //inline style for export to excel
                            <tr style={rowStyle} key={i}>
                                {
                                    columns.map(col => {
                                        const currentVal = Object.keys(row).find(el => col.id === el);
                                        if (currentVal) {
                                            // Small hack to allow us render URLs that have been sent as an object
                                            // this should be reworked as it's not nice
                                            let value = row[currentVal];
                                            if(value && value.url) {
                                                value = value.url;
                                            }
                                            return <td key={col.id}>{value}</td>;
                                        }
                                        return <td key={col.id}>&nbsp;</td>;
                                    })}
                            </tr>
                        );
                    })}
                    </tbody>
                </table>

                <ExcelIcon className={ExcelImportStyles.icon} onClick={() => this.exportToXLS()}/>
            </React.Fragment>
        );
    }
};
