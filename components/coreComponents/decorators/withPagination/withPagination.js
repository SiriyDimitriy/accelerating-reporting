import React from 'react';
import PaginationWrapperStyles from './withPagination.less';
import PropTypes from 'prop-types';
import Pagination from '../../../simple/Pagination/Pagination';
import Select from '../../../simple/Select/Select';

const withPagination = (options) => (WrappedComponent) => {
    return class extends React.PureComponent {
        constructor(props) {
            super(props);
            this.state = {
                itemsPerPage: localStorage.getItem('itemsOnPage') ? JSON.parse(localStorage.getItem('itemsOnPage')):15,
            };
            this.onSelectHandler = this.onSelectHandler.bind(this);
        }

        onSelectHandler(value) {
            const {setDisplayedEntitiesNumber} = this.props;

            this.setState(() => {
                localStorage.setItem('itemsOnPage', JSON.stringify(value));

                return {
                    itemsPerPage: value
                };
            });

            setDisplayedEntitiesNumber && setDisplayedEntitiesNumber(parseInt(value, 10));
        }

        paginationHandler = ({page}) => {
            const {handleChangePage} = this.props;
            handleChangePage(page);
        };

        render() {
            const {pagesPagination, displayedEntriesPagination} = options;
            const {itemsCount, firstResult, itemsOnPage, entities} = this.props;
            const {itemsPerPage} = this.state;
            let {translations, currentPage} = this.props;
            console.log(itemsPerPage);

            const pageCount = Math.ceil(itemsCount / itemsOnPage);
            if (!(currentPage >= 0)) {
                currentPage = Math.ceil(firstResult / itemsOnPage);
            }

            translations = translations || {
                paginableMaxResults: 'Display entries',
            };
            const {
                next, last, pagerFirst, pagerPrevious, paginationOf, paginableMaxResults,
            } = translations;

            const paginationTitles = {
                first: pagerFirst,
                previous: pagerPrevious,
                next: next,
                last: last,
                wordOf: paginationOf,
            };

            return (
                <React.Fragment>
                    {entities && entities.entities && <div className={PaginationWrapperStyles.controls}>
                        <section>
                            {pagesPagination && <Pagination itemsOnPage={itemsPerPage}
                                                            currentPage={currentPage}
                                                            pageCount={pageCount}
                                                            itemsTotalAmount={itemsCount}
                                                            pagesDisplayedAroundSelected={3}
                                                            showPagesRange={true}
                                                            showPages={true}
                                                            fetchData={this.paginationHandler}
                                                            customTitles={paginationTitles}
                            />}
                        </section>
                        <section className={PaginationWrapperStyles.right_side}>
                            {displayedEntriesPagination && <Select label={`${paginableMaxResults}:`}
                                                                   options={[
                                                                       {value: 15, label: 15},
                                                                       {value: 25, label: 25},
                                                                       {value: 50, label: 50},
                                                                       {value: 100, label: 100},
                                                                   ]}
                                                                   optionsToTop = {false}
                                                                   onSelectHandler={this.onSelectHandler}
                                                                   defaultValue={itemsPerPage}
                            />}
                        </section>
                    </div>}

                    <WrappedComponent {...this.props}/>

                    {entities && entities.entities && <div id='footer'
                        className={`${PaginationWrapperStyles.controls} ${PaginationWrapperStyles.footer}`} >
                        <section>
                            {pagesPagination && <Pagination itemsOnPage={itemsPerPage}
                                                            currentPage={currentPage}
                                                            pageCount={pageCount}
                                                            itemsTotalAmount={itemsCount}
                                                            pagesDisplayedAroundSelected={3}
                                                            showPagesRange={true}
                                                            showPages={true}
                                                            fetchData={this.paginationHandler}
                                                            customTitles={paginationTitles}
                            />}
                        </section>

                        <section className={PaginationWrapperStyles.right_side}>
                            {displayedEntriesPagination && <Select label={`${paginableMaxResults}:`}
                                                                   options={[
                                                                       {value: 15, label: 15},
                                                                       {value: 25, label: 25},
                                                                       {value: 50, label: 50},
                                                                       {value: 100, label: 100},
                                                                   ]}
                                                                   optionsToTop = {true}
                                                                   onSelectHandler={this.onSelectHandler}
                                                                   defaultValue={itemsPerPage}
                            />}
                        </section>
                    </div>}
                </React.Fragment>
            );
        }
    };
};

export {withPagination};

withPagination.propTypes = {
    pagesPagination: PropTypes.bool,
    displayedEntriesPagination: PropTypes.bool,
};

withPagination.defaultProps = {
    pagesPagination: true,
    displayedEntriesPagination: true,
    configuredTableColumns: false,
};
