import React from 'react';
import HeaderStyles from './Header.less';
import GUILngSwitcher from '../GUILngSwitcher/GUILngSwitcher';
import {Link} from 'react-router-dom';
// import InfoIcon from '../../../assets/svg/information-icon.svg';
// import HelpIcon from '../../../assets/svg/help-icon-new.svg';

const Header = () => {
    return (
        <div className={HeaderStyles.header}>
            <div className={HeaderStyles.left_side}>
                <Link to={'/'}>
                    <div className={HeaderStyles.title}>Accelerating reporting</div>
                </Link>
            </div>
            <div className={HeaderStyles.right_side}>
                <GUILngSwitcher/>
                <img className={HeaderStyles.logo} src='/out/assets/jpg/logo.png' alt={'logo'}/>
            </div>
        </div>
    );
};


export default Header;
