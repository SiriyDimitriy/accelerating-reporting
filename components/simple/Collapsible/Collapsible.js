import React from 'react';
import CollapsibleStyles from './Collapsible.less';
import RadioButton from '../../../components/simple/RadioButton/RadioButton';
import ChevronUp from '../../../assets/svg/select-chevron-up.svg';
import ChevronDown from '../../../assets/svg/select-chevron-down.svg';
import ClearSelectionsIcon from '../../../assets/svg/clear_search.svg';
import MultiSelectItem from '../MultiSelectItem/MultiSelectItem';
import SearchControlsStyles from '../../../pages/MainPage/SearchControls/SearchControls.less';
import InputElement from '../InputElement/InputElement';
import {getCookie} from '../../../utils/cookiesUtils';
import httpService from '../../../utils/httpService';

const baseApiUrl = process.env.SEARCHENGINE_API_URL;

class Collapsible extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: true,
        };

        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.onAutocompleteSelectHandler = this.onAutocompleteSelectHandler.bind(this);
        this.onClearSelectionHandler = this.onClearSelectionHandler.bind(this);
    }

    onCollapseHandler = () => {
        this.setState((state) => {
            return {collapsed: !state.collapsed};
        });
    };

    onClearSelectionHandler = (e) => {
        e.stopPropagation();
        const {id, clearSingleSelectedFacet} = this.props;

        clearSingleSelectedFacet && clearSingleSelectedFacet(id);
    };

    handleChangeInput(val) {
        const {selectedDocType, selectedContentLanguage} = this.props;
        const {id} = this.props;

        const requestObject = {
            'documentType': selectedDocType === 'ALL' ? '' : selectedDocType,
            'language': selectedContentLanguage,
            'locale': getCookie('locale'),
        };

        return httpService.doRequest('POST', `${baseApiUrl}/documents/dynamicFieldSuggest?dynamicFieldId=${id}&part=${val}`, requestObject);
    }

    onAutocompleteSelectHandler(val) {
        const {addFacetValue, id} = this.props;

        addFacetValue && addFacetValue(id, val);
    }

    render() {
        const {field, values, type, dataType, title, toggleFacet, id, selectedValues} = this.props;
        const {collapsed} = this.state;

        const isSelectedFacets = selectedValues && Object.keys(selectedValues).length !== 0;

        return (
            <div className={CollapsibleStyles.section}>
                <div onClick={this.onCollapseHandler} className={CollapsibleStyles.header_section}>
                    <div className={CollapsibleStyles.title}>{title}</div>
                    <ClearSelectionsIcon className={CollapsibleStyles.clear_selections_icon}
                                         style={isSelectedFacets ? {} : {cursor: 'not-allowed', opacity: 0.5}}
                                         onClick={this.onClearSelectionHandler}
                    />
                    <span className={CollapsibleStyles.arrow}>
                    {collapsed ? <ChevronUp className={CollapsibleStyles.icon}/> :
                        <ChevronDown className={CollapsibleStyles.icon}/>}
                </span>
                </div>

                {Object.keys(values).map((value) => {
                    const count = values[value].count;
                    // const selected = values[value].selected;
                    if (collapsed) {
                        switch (type) {
                            case 'single-select': {
                                return <RadioButton label={value}
                                                    extraInfo={count}
                                                    checked={selectedValues && selectedValues.find(val => val === value)}
                                                    id={value}
                                                    key={value}
                                                    group={field}
                                                    onClickHandler={() => toggleFacet(id, value, dataType, false)}
                                />;
                            }

                            case 'multi-select': {
                                return <MultiSelectItem title={value}
                                                        extraInfo={count}
                                                        id={value}
                                                        key={value}
                                                        selected={selectedValues && selectedValues.find(val => val === value)}
                                                        onClickHandler={() => toggleFacet(id, value, dataType, false)}
                                />;
                            }

                            case 'interval': {
                                return <MultiSelectItem title={value}
                                                        extraInfo={count}
                                                        id={value}
                                                        key={value}
                                                        selected={selectedValues && selectedValues.find(val => val === value)}
                                                        onClickHandler={() => toggleFacet(id, value, dataType, true)}
                                />;
                            }

                            default:
                                return <span>Unknown type</span>;
                        }
                    }
                    return null;
                })}
                {collapsed && <div className={CollapsibleStyles.less_used_values_block}>
                    <span className={CollapsibleStyles.less_used_values_block_label}>Less used values:</span>
                    <InputElement
                        onChangeHandler={this.handleChangeInput}
                        onAutocompleteSelectHandler={this.onAutocompleteSelectHandler}
                        className={SearchControlsStyles.input_field}
                    />
                </div>}
            </div>
        );
    }
}

export default Collapsible;