import React from 'react';
import PropTypes from 'prop-types';
import TooltipStyles from './Tooltip.less';
import TooltipContainer from './src/TooltipContainer';
import PortalRender from './src/PortalRenderingTTContainer';

class Tooltip extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayTooltip: false,
            top: null,
            left: null
        };
    }

    calculateTooltipPosition = () => {
        const bodyRect = document.body.getBoundingClientRect();
        const targetRect = this.target.getBoundingClientRect();
        return {
            top: targetRect.top + targetRect.height - bodyRect.top,
            left: targetRect.left - bodyRect.left
        };
    };

    checkOnHide = () => {
        const {onHide} = this.props;
        if (onHide) {
            onHide();
        }
    };

    outsideClickListener = (event) => {
        const {id} = this.props;
        if (!document.getElementById(id).contains(event.target)) {
            this.setState({displayTooltip: false});
            this.removeClickListener();
        }
    };

    removeClickListener = () => {
        this.checkOnHide();
        document.removeEventListener('click', this.outsideClickListener);
    };

    hideTooltip = () => {
        this.setState({displayTooltip: false});
        this.removeClickListener();
    };

    toggleTooltip = () => {
        const {portalRendering} = this.props;
        const {displayTooltip} = this.state;
        const tooltipPosition = portalRendering ? this.calculateTooltipPosition() : {};
        if (displayTooltip) {
            this.removeClickListener();
        } else {
            document.addEventListener('click', this.outsideClickListener);
        }
        this.setState({displayTooltip: !displayTooltip, ...tooltipPosition});
    };

    render() {
        const {target, portalRendering, ...restProps} = this.props;
        const {displayTooltip, top, left} = this.state;
        return (
            <span className={TooltipStyles.tooltip} ref={tooltip => this.tooltip = tooltip}>
                <span className={TooltipStyles.tooltip_trigger}
                      onClick={this.toggleTooltip}
                      ref={target => this.target = target}
                >{target}
                </span>
                {displayTooltip && !portalRendering && <TooltipContainer {...restProps} hideTooltip={this.hideTooltip}/>}
                {displayTooltip && portalRendering && <PortalRender top={top} left={left} {...restProps} hideTooltip={this.hideTooltip}/>}
            </span>
        );
    }
}

Tooltip.propTypes = {
    onHide: PropTypes.func,
    position: PropTypes.string,
    children: PropTypes.any,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    // portalRendering - prevents hiding tooltip with 'overflow: hidden' css rule,
    // renders tooltip outside current container
    portalRendering: PropTypes.bool
};

export default Tooltip;
