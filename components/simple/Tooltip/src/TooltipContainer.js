import React from 'react';
import TooltipStyles from '../Tooltip.less';
import classNames from 'classnames/bind';

let cx = classNames.bind(TooltipStyles);

class TooltipContainer extends React.Component {
    render() {
        const {position, children, hideTooltip, id} = this.props;
        const tooltipContainerClass = cx({
            'tooltip_container': true,
            [`tooltip_${position}`]: true
        });

        return (
            <div className={tooltipContainerClass} id={id}>
                {/*<div className={TooltipStyles['tooltip-message']}>{children}</div>*/}
                <div className={TooltipStyles.tooltip_message}>
                    {React.cloneElement(children, {hideTooltip: hideTooltip})}
                </div>
            </div>
        );
    }
}

export default TooltipContainer;
