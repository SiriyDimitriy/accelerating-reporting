import React from 'react';
import ReactDOM from 'react-dom';
import TooltipContainer from './TooltipContainer';

const PortalRender = ({top, left, ...restProps}) => {
    const style = {
        position: 'absolute',
        top,
        left
    };
    return ReactDOM.createPortal(
        <div style={style}>
            <TooltipContainer {...restProps}/>
        </div>,
        document.body
    );
};

export default PortalRender;
