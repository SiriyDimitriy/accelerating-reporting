import React from 'react';
import HorizontalDividerStyles from './HorizontalDivider.less';

const HorizontalDivider = () => {
    return (
        <div className={HorizontalDividerStyles.divider}/>
    );
};

export default HorizontalDivider;