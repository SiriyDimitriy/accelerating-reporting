import React from 'react';
import PropTypes from 'prop-types';

const BasePopup = () => (WrappedPopup) => {
    WrappedPopup.propTypes = {
        show: PropTypes.bool,
    };

    return class Popup extends React.PureComponent {
        render() {
            const {show} = this.props;

            return show ? <WrappedPopup {...this.props}/> : null;
        }
    };
};

export default BasePopup;
