import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import PopupStyles from './Popup.less';
import BasePopup from './decorators/BasePopup';

@BasePopup()
class Popup extends React.PureComponent {
    componentDidMount() {
        // const {allowBackdropToClose} = this.props;
        document.addEventListener('click', this.outsideClickListener);
        // document.addEventListener('keydown', this.checkPressedKey);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.outsideClickListener);
        // document.removeEventListener('keydown', this.checkPressedKey);
    }

    closePopup = () => {
        const {closeHandler} = this.props;
        if (closeHandler) {
            closeHandler();
        }
    };

    outsideClickListener = (event) => {
        const {id} = this.props;
        if (event.target.id !== id) return;
        if (!this.popup.contains(event.target)) {
            this.closePopup();
        }
    };

    // checkPressedKey = (e) => {
    //     console.log(e);
    //     if (e.key === 'Escape') {
    //         this.closePopup();
    //     }
    // };

    render() {
        const {children, headerText, id} = this.props;
        return ReactDOM.createPortal(
            <div className={PopupStyles.popup_mask}>
                <div className={PopupStyles.popup_bg} id={id}/>
                <div className={PopupStyles.popup} ref={popup => this.popup = popup}>
                    <div className={PopupStyles.header}>
                        {headerText ? headerText : ' '}
                    </div>
                    <div className={PopupStyles.content}>
                        {children}
                    </div>
                </div>
            </div>,
            document.body
        );
    }
}

Popup.propTypes = {
    closeHandler: PropTypes.func.isRequired,
    headerText: PropTypes.string,
    children: PropTypes.any,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};

export default Popup;
