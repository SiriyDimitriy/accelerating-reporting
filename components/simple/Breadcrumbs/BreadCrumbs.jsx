import React from 'react';
import {Link} from 'react-router-dom';
import BreadCrumbsStyles from './BreadCrumbs.less';

const BreadCrumbs = ({sections}) => {

    if (!sections || !sections.length) {
        return null;
    }
    return (
        <div className={BreadCrumbsStyles.breadcrumbs}>
            {sections.map((section, i) => {
                const key = 'section_' + i;
                if (i === sections.length - 1) {
                    return <div key={key}>{section.label}</div>;
                }
                return <React.Fragment key={key}>
                    <Link className={BreadCrumbsStyles.section} to={section.path}>{section.label}</Link>
                    <span className={BreadCrumbsStyles.arrow}/>
                </React.Fragment>;
            })}
        </div>
    );
};

export default BreadCrumbs;
