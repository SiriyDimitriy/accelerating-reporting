import React from 'react';
import PropTypes from 'prop-types';
import PaginationStyles from './Pagination.less';
import classNames from 'classnames/bind';
import LeftArrowIcon from '../../../assets/svg/arrow-to-left.svg';
import RightArrowIcon from '../../../assets/svg/arrow-to-right.svg';
import DoubleRightArrowIcon from '../../../assets/svg/double-chevron-to-right.svg';
import DoubleLeftArrowIcon from '../../../assets/svg/double-chevron-to-left.svg';

let cx = classNames.bind(PaginationStyles);

const Pagination = (props) => {
    const {
        pageCount,
        currentPage,
        itemsOnPage,
        pagesDisplayedAroundSelected,
        fetchData,
        queryObject,
        style,
        showPages,
        showPagesRange,
        itemsTotalAmount,
    } = props;

    const isFirstPage = (currentPage) => {
        return currentPage === 0;
    };

    const isLastPage = (pageCount, currentPage) => {
        return currentPage === pageCount - 1;
    };

    let pages = [];

    if (showPages) {
        const isPageDisplayed = (page) => {
            return !(((currentPage + pagesDisplayedAroundSelected + 1) < pageCount)
                && (page > (currentPage + pagesDisplayedAroundSelected + 1))
                && (page < pageCount))
                && !(
                    ((currentPage - pagesDisplayedAroundSelected - 1) > 1)
                    && (page < (currentPage - pagesDisplayedAroundSelected - 1))
                    && (page > 1)
                );
        };

        for (let i = 1; i < pageCount + 1; i++) {
            let pageClass = cx({
                'page': true,
                'selected_page': i === currentPage + 1,
            });

            if (((currentPage + pagesDisplayedAroundSelected + 1) < pageCount)
                && (i === (currentPage + pagesDisplayedAroundSelected + 1))) {
                pages.push(<span className={PaginationStyles.points} key={i}>...</span>);
            } else if (((currentPage - pagesDisplayedAroundSelected - 1) > 1)
                && (i === (currentPage - pagesDisplayedAroundSelected - 1))) {
                pages.push(<span className={PaginationStyles.points} key={i}>...</span>);
            } else if (isPageDisplayed(i)) {
                pages.push(<div key={i} title={`Page ${i}`} className={pageClass}
                                onClick={() => fetchData({pageSize: itemsOnPage, page: i - 1, ...queryObject})}
                >
                    {i}
                </div>);
            }
        }
    }

    let startRange, endRange;

    if (showPagesRange) {
        endRange = itemsOnPage * (currentPage + 1);
        startRange = endRange - itemsOnPage + 1;
    }

    if (pageCount === 0 || pageCount === 1) {
        return null;
    }

    // additional wrapping of icon by html-element (span) needed for translated title
    return (
        <section style={style} className={PaginationStyles.section}>
            {showPagesRange && <div>
                {startRange} - {endRange} of {itemsTotalAmount}
            </div>}
            {
                !isFirstPage(currentPage)
                && <React.Fragment>
                    <span title={'First page'} className={PaginationStyles.pagination_arrow_wrapper}>
                        <DoubleLeftArrowIcon
                            className={PaginationStyles.pagination_arrow}
                            onClick={() => fetchData({
                                pageSize: itemsOnPage,
                                page: 0, ...queryObject
                            })}
                        />
                    </span>
                    <span title={'Previous page'} className={PaginationStyles.pagination_arrow_wrapper}>
                        <LeftArrowIcon
                            className={PaginationStyles.pagination_arrow}
                            onClick={() => fetchData({
                                pageSize: itemsOnPage,
                                page: currentPage - 1, ...queryObject
                            })}
                        />
                    </span>
                </React.Fragment>
            }
            {showPages && <div className={PaginationStyles.pages}>
                {pages}
            </div>}
            {!isLastPage(pageCount, currentPage)
            && <React.Fragment>
                <span title={'Next page'} className={PaginationStyles.pagination_arrow_wrapper}>
                    <RightArrowIcon
                        className={PaginationStyles.pagination_arrow}
                        onClick={() => fetchData({
                            pageSize: itemsOnPage,
                            page: currentPage + 1, ...queryObject
                        })}
                    />
                </span>
                <span title={'Last page'} className={PaginationStyles.pagination_arrow_wrapper}>
                    <DoubleRightArrowIcon
                        className={PaginationStyles.pagination_arrow}
                        onClick={() => fetchData({
                            pageSize: itemsOnPage,
                            page: pageCount - 1, ...queryObject
                        })}
                    />
                </span>
            </React.Fragment>}
        </section>
    );
};

Pagination.propTypes = {
    showPagesRange: PropTypes.bool,
    showPages: PropTypes.bool,
    pageCount: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    itemsOnPage: PropTypes.number,
    itemsTotalAmount: PropTypes.number,
    pagesDisplayedAroundSelected: PropTypes.number.isRequired
};

export default Pagination;
