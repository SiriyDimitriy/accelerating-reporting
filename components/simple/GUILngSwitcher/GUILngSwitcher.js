import React from 'react';
import Styles from './GUILngSwitcher.less';
import Select from '../Select/Select';
import {getCookie, setCookie} from '../../../utils/cookiesUtils';
import {withTranslation} from 'react-i18next';
import {createStructuredSelector} from 'reselect';
import {
    makeSelectGUILanguages,
} from '../../../selectors/commonSelectors';
import connect from 'react-redux/es/connect/connect';

const mapStateToProps = createStructuredSelector({
    GUILanguages: makeSelectGUILanguages(),
});

@connect(mapStateToProps)
@withTranslation()
class GUILngSwitcher extends React.Component {
    constructor(props) {
        super(props);
        const currentLanguage = getCookie('locale') || (props.GUILanguages && props.GUILanguages[0] && props.GUILanguages[0].value);
        this.state = {
            currentLanguage: currentLanguage,
        };

    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        const {GUILanguages, i18n} = this.props;

        if (nextProps.GUILanguages !== GUILanguages) {
            const currentLanguage = nextProps.GUILanguages && nextProps.GUILanguages[0] && nextProps.GUILanguages[0].value;

            this.setState({currentLanguage: currentLanguage});

            i18n.changeLanguage(currentLanguage).then(() => {
                setCookie('locale', currentLanguage, 10000);
            });
        }
    }

    languageChangeHandler = (name) => {
        const {i18n} = this.props;
        this.setState({currentLanguage: name}, () => setCookie('locale', name, 10000));
        i18n.changeLanguage(name);
    };

    render() {
        const {GUILanguages} = this.props;
        const {currentLanguage} = this.state;

        return (
            <div className={Styles.wrapper}>
                <Select onSelectHandler={this.languageChangeHandler}
                        options={GUILanguages}
                        defaultValue={currentLanguage}
                />
            </div>
        );
    }
}

export default GUILngSwitcher;
