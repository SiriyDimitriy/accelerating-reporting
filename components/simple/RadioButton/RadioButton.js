import React from 'react';
import RadioButtonStyles from './RadioButton.less';
import classNames from 'classnames/bind';

let cx = classNames.bind(RadioButtonStyles);

class RadioButton extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {label, id, extraInfo, group, onClickHandler, checked} = this.props;

        const labelClass = cx({
            'label': true,
            'label_checked': checked,
            'label_unchecked': !checked,
        });

        return (
            <section className={RadioButtonStyles.wrapper} onClick={onClickHandler}>
                <input className={RadioButtonStyles.input} type='radio' id={id} name={group} checked={checked}/>
                <div className={labelClass}>{label}</div>
                <div className={RadioButtonStyles.extra_info}>{`(${extraInfo})`}</div>
            </section>
        );
    }
}

export default RadioButton;
