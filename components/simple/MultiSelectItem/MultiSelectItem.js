import React from 'react';
import Checkbox from '../Checkbox/Checkbox';
import MultiSelectStyles from './MultiSelectItem.less';

class MultiSelectItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.selected,
        };
    }

    handleCheckBoxState = () => {
        const {checked} = this.state;
        const {onClickHandler} = this.props;

        this.setState({checked: !checked});
        onClickHandler();
    };

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.selected) {
            this.setState({checked: true});
        } else {
            this.setState({checked: false});
        }
    }

    render() {
        const {title, id, disabled, extraInfo} = this.props;
        const {checked} = this.state;
        return (
            <div className={MultiSelectStyles.item}>
                <div className={MultiSelectStyles.checkbox}>
                    <Checkbox checked={checked}
                              id={id}
                              disabled={disabled}
                              onToggle={this.handleCheckBoxState}/>
                </div>
                <div className={MultiSelectStyles.title}>{title}</div>
                <div className={MultiSelectStyles.extra_info}>{`(${extraInfo})`}</div>
            </div>
        );
    }
}

export default MultiSelectItem;