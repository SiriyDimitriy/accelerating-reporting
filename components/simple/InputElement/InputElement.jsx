import React from 'react';
import classNames from 'classnames/bind';
import InputStyles from './InputElement.less';

let cx = classNames.bind(InputStyles);

class InputElement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value || '',
            valuesList: null,
            focusedValue: null,
        };

        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onAutocompleteClickHandler = this.onAutocompleteClickHandler.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);

        this.autocompleteContainer = React.createRef();
    }

    onKeyDown(e) {
        const {valuesList, focusedValue} = this.state;

        switch (e.key) {
            case 'Escape':
            case 'Tab':
                if (valuesList) {
                    e.preventDefault();
                    this.setState({
                        valuesList: null,
                    });
                }
                break;
            case 'Enter':
                e.stopPropagation();
                this.setState({value: focusedValue && focusedValue.replace(/<\/?[^>]+(>|$)/g, '')});
                break;
            case 'ArrowDown':
                e.preventDefault();
                this.setState(prevState => {
                    let {focusedValue} = prevState;
                    const prevFocusedValueIndex = valuesList.indexOf(focusedValue);

                    if (prevFocusedValueIndex < valuesList.length - 1) {
                        focusedValue = valuesList[prevFocusedValueIndex + 1];

                        return {
                            // values: [options[focusedValue].value],
                            focusedValue,
                        };
                    }
                });
                break;
            case 'ArrowUp':
                e.preventDefault();
                this.setState(prevState => {
                    let {focusedValue} = prevState;
                    const prevFocusedValueIndex = valuesList.indexOf(focusedValue);

                    if (prevFocusedValueIndex !== 0 && prevFocusedValueIndex <= valuesList.length - 1) {
                        focusedValue = valuesList[prevFocusedValueIndex - 1];

                        return {
                            // values: [options[focusedValue].value],
                            focusedValue,
                        };
                    }
                });
                break;
            // default:
            //     if (/^[a-z0-9]$/i.test(e.key)) {
            //         const char = e.key;
            //
            //         clearTimeout(this.timeout);
            //
            //         this.setState((prevState) => {
            //             const typed = prevState.typed || '' + char;
            //             const regex = new RegExp(`^${typed}`, 'i');
            //             // const index = options.findIndex(option => re.test(option.value));
            //             // let regex = '/^' + typed + '/i'; //IE don't support new RegExp
            //             let index;
            //             let optionsList = [];
            //
            //             index = options.findIndex(option => regex.test(option.value));
            //
            //             return {
            //                 values: (options[index] ? [options[index].value] : []) || (optionsList[index] ? [optionsList[index].value] : []),
            //                 focusedValue: index,
            //             };
            //
            //         });
            //     }
            //     break;
        }
    }

    componentDidMount() {
        window.addEventListener('click', (e) => {
            if (this.autocompleteContainer && this.autocompleteContainer.current && !this.autocompleteContainer.current.contains(e.target)) {
                this.setState({valuesList: null});
            }
        });
    }

    onChangeHandler(e) {
        const {onChangeHandler, onAutocompleteSelectHandler} = this.props;
        this.setState({value: e.target.value});
        if (onAutocompleteSelectHandler && onChangeHandler && (e.target.value !== '')) {
            onChangeHandler && onChangeHandler(e.target.value).then(res => {
                this.setState({valuesList: res});
            });
        } else {
            onChangeHandler && onChangeHandler(e.target.value);
        }
    }

    onAutocompleteClickHandler(e) {
        const {onAutocompleteSelectHandler} = this.props;
        const value = e.target.innerHTML.replace(/<\/?[^>]+(>|$)/g, '');

        this.setState({
            value: value,
            valuesList: null
        }, () => onAutocompleteSelectHandler && onAutocompleteSelectHandler(value));
    }

    render() {
        const {placeholder, type, onBlur, className, width, hasError, errorMessage} = this.props;
        const {value, valuesList, focusedValue} = this.state;

        let inputFieldClass = cx({
            'input_field': true,
            input_error: hasError,
            [className]: className,
        });

        let inputClass = cx({
            'input': true,
        });

        return (
            <div className={inputFieldClass} style={{width: width}} onKeyDown={this.onKeyDown}>
                <input
                    ref={el => (this.input = el)}
                    type={type || 'text'}
                    autoComplete={'new-password'} // disable autofill
                    placeholder={placeholder}
                    className={inputClass}
                    value={value}
                    onChange={this.onChangeHandler}
                    onBlur={onBlur}
                />
                {hasError && <span
                    className={InputStyles.input_field_error}>{errorMessage || 'This field contains incorrect value'}</span>}
                {valuesList
                && <div className={InputStyles.autocomplete_section} ref={this.autocompleteContainer}>
                    {valuesList.map(el => <div key={el}
                                               className={InputStyles.autocomplete_item}
                                               style={el === focusedValue ? {backgroundColor: '#4EBDEE'} : {}}
                                               onClick={this.onAutocompleteClickHandler}
                                               dangerouslySetInnerHTML={{__html: el}}/>)}
                </div>}
            </div>
        );
    }
}

export default InputElement;
