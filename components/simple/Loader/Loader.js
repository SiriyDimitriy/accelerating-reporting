import React from 'react';
import PropTypes from 'prop-types';
import LoaderStyle from './Loader.less';
import classNames from 'classnames/bind';

let cx = classNames.bind(LoaderStyle);

const Loader = ({dotsCount, dotSize, color, radius}) => {
    const dots = new Array(dotsCount);
    dots.fill(1);
    const loaderRadius = {width: radius, height: radius};
    const dotStyle = {width: dotSize, height: dotSize, backgroundColor: color};

    return (
        <section className={LoaderStyle.loader_section} style={loaderRadius}>
            {dots.map((el, i) => {
                const circleNum = 'sk_circle' + (i + 1);
                const circleCss = cx({
                    'sk_circle': true,
                    [circleNum]: true
                });
                // SPAN (it may be any element) - because it is impossible to apply inline style to ::before selector
                return (
                    <div className={circleCss} key={circleNum}>
                        <span style={dotStyle}/>
                    </div>
                );
            })}
        </section>
    );
};


Loader.propTypes = {
    color: PropTypes.string.isRequired,
    dotsCount: PropTypes.number.isRequired,
    dotSize: PropTypes.string.isRequired,
    radius: PropTypes.string.isRequired,
};

Loader.defaultProps = {
    color: 'red',
    dotsCount: 12,
    radius: '60px',
    dotSize: '15%'
};

export {
    Loader as default,
};
