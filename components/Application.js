import React from 'react';
import {Switch, Route, withRouter} from 'react-router-dom';
import {initHttpServer} from '../utils/httpService';
import Alert from 'react-s-alert';
import {Trans, withTranslation} from 'react-i18next';
import {
    setHistory,
    // setTrans,
} from '../utils/context_variables';
import SearchPage from '../pages/SearchPage/SearchPage';
import {connect} from 'react-redux';
import {getInitialData} from '../actions/commonActions';
import Header from './simple/Header/Header';

const mapDispatchToProps = dispatch => ({
    getInitialData: () => dispatch(getInitialData()),
});

@withTranslation()
@withRouter
@connect(null, mapDispatchToProps)
class AppContent extends React.Component {
    constructor(props) {
        super(props);
        this.initContext();
    }

    componentDidMount() {
        const {getInitialData} = this.props;

        getInitialData();
    }

    initContext() {
        const {history} = this.props;

        setHistory(history);
        // setTrans(t);
        initHttpServer(history);
    }

    render() {
        return (
            <React.Fragment>
                <Header/>
                <Switch>
                    <Route path='/' component={SearchPage}/>
                </Switch>

                <Alert stack={{limit: 6}}
                       key='alert'
                       effect='scale'
                       timeout={5000}
                       position='top-right'
                />
            </React.Fragment>
        );
    }
}

export default AppContent;
