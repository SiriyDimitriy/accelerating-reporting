import {
    GET_ENTITIES,
    GET_INITIAL_DATA, REORDER_COLUMN,
    SET_DATE_FROM,
    SET_DATE_TO, SET_PAGE_NUMBER, SET_PAGE_SIZE,
    SET_USER_STRING,
    TOGGLE_LOADER, TOGGLE_SEARCH_ACTIVITY
} from '../constants/actionTypes/commonConstants';

export function toggleLoader(showLoader) {
    return {
        type: TOGGLE_LOADER,
        showLoader,
    };
}

export function getInitialData() {
    return {
        type: GET_INITIAL_DATA,
    };
}

export function getEntities() {
    return {
        type: GET_ENTITIES,
    };
}

export function setDateFrom(date) {
    return {
        type: SET_DATE_FROM,
        date,
    };
}

export function setDateTo(date) {
    return {
        type: SET_DATE_TO,
        date,
    };
}

export function setUserString(string) {
    return {
        type: SET_USER_STRING,
        string,
    };
}

export function setPageSize(pageSize) {
    return {
        type: SET_PAGE_SIZE,
        pageSize,
    };
}

export function setPageNumber(pageNumber) {
    return {
        type: SET_PAGE_NUMBER,
        pageNumber,
    };
}

export function reorderColumn(startIndex, destinationIndex, dataType) {
    return {
        type: REORDER_COLUMN,
        startIndex,
        destinationIndex,
        dataType
    };
}

export function toggleSearchActivity(searchIsDisabled) {
    return {
        type: TOGGLE_SEARCH_ACTIVITY,
        searchIsDisabled,
    };
}
