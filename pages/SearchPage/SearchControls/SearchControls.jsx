import React from 'react';
import SearchControlsStyles from './SearchControls.less';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
// import {
//     makeSelectActiveDynamicField, makeSelectActivityOfSearch,
//     makeSelectContentLanguages,
//     makeSelectDynamicFields, makeSelectedFacetsList, makeSelectedRelevantFields, makeSelectSearchString,
//     makeSelectSelectedContentLanguage,
// } from '../../../selectors/entities/commonSelectors';
import InputElement from '../../../components/simple/InputElement/InputElement';
import i18next from 'i18next';
import SearchIcon from '../../../assets/svg/search.svg';
import DatePicker from 'react-datepicker';

// CSS Modules, react-datepicker-cssmodules.css
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

import {
    getEntities,
    // setContentLanguage,
    // setSearchString,
    // toggleSearchActivity,
    // setPageNumber,
    setDateFrom,
    setDateTo, setUserString,
} from '../../../actions/commonActions';
// import httpService from '../../../utils/httpService';
// import {
//     getEntities,
// } from '../../../actions/entities/searchActions';
import {getCookie} from '../../../utils/cookiesUtils';
import {makeSelectDateFrom, makeSelectDateTo} from '../../../selectors/commonSelectors';
import HorizontalDivider from '../../../components/simple/HorizontalDivider/HorizontalDivider';

const mapStateToProps = createStructuredSelector({
    // contentLanguages: makeSelectContentLanguages(),
    // selectedContentLanguage: makeSelectSelectedContentLanguage(),
    // searchString: makeSelectSearchString(),
    // searchIsDisabled: makeSelectActivityOfSearch(),
    dateFrom: makeSelectDateFrom(),
    dateTo: makeSelectDateTo(),
});

const mapDispatchToProps = dispatch => ({
    // setContentLanguage: (lng) => dispatch(setContentLanguage(lng)),
    setUserString: (string) => dispatch(setUserString(string)),
    getEntities: () => dispatch(getEntities()),
    setDateFrom: (date) => dispatch(setDateFrom(date)),
    setDateTo: (date) => dispatch(setDateTo(date)),
    // restartPageNumber: () => dispatch(setPageNumber(0)),
});

// @withRouter
@connect(mapStateToProps, mapDispatchToProps)
class SearchControls extends React.PureComponent {
    constructor(props) {
        super(props);

        // this.state = {
        //     GUIlanguage: getCookie('locale'),
        // };

        this.searchStringChangeHandler = this.searchStringChangeHandler.bind(this);
        // this.onSearchClickHandler = this.onSearchClickHandler.bind(this);
        // this.onAutocompleteSelectHandler = this.onAutocompleteSelectHandler.bind(this);

        this.dateToChangeHandler = this.dateToChangeHandler.bind(this);
        this.dateFromChangeHandler = this.dateFromChangeHandler.bind(this);
    }

    onSearchClickHandler = () => {
        const {getEntities, searchIsDisabled, restartPageNumber} = this.props;

        if (searchIsDisabled) return;
        // restartPageNumber();
        getEntities();
    };

    searchStringChangeHandler = (val) => {
        const {setUserString} = this.props;

        setUserString && setUserString(val);
    };

    dateToChangeHandler = (date) => {
        const {setDateTo} = this.props;

        setDateTo && setDateTo(date);
    };

    dateFromChangeHandler = (date) => {
        const {setDateFrom} = this.props;

        setDateFrom && setDateFrom(date);
    };

    // onAutocompleteSelectHandler = (val) => {
    //     const {setSearchString} = this.props;
    //
    //     setSearchString(val);
    // };

    render() {
        const {setSearchString, searchIsDisabled, dateFrom, dateTo} = this.props;

        // const {GUIlanguage} = this.state;

        const searchBtnClassName = (searchIsDisabled) ? ` ${SearchControlsStyles.disabled}` : '';

        return (
            <React.Fragment>
                <div className={SearchControlsStyles.search_panel}>
                    {/*{contentLanguages && <div className={SearchControlsStyles.content_language_select}>*/}
                    {/*<Select onSelectHandler={this.contentLanguageChangeHandler}*/}
                    {/*options={contentLanguages}*/}
                    {/*defaultValue={selectedContentLanguage}*/}
                    {/*/>*/}
                    {/*</div>}*/}

                    <div className={SearchControlsStyles.date_field}>
                        <section className={SearchControlsStyles.date_field_label}>Date from:</section>
                        <DatePicker selected={dateFrom}
                                    onChange={this.dateFromChangeHandler}
                                    showTimeSelect
                                    timeFormat='HH:mm'
                                    timeIntervals={15}
                                    timeCaption='time'
                                    dateFormat='MMMM d, yyyy h:mm aa'
                                    selectsStart
                                    startDate={dateFrom}
                                    endDate={dateTo}
                        />
                    </div>

                    <HorizontalDivider/>

                    <div className={SearchControlsStyles.date_field}>
                        <section className={SearchControlsStyles.date_field_label}>Date to:</section>
                        <DatePicker selected={dateTo}
                                    onChange={this.dateToChangeHandler}
                                    showTimeSelect
                                    timeFormat='HH:mm'
                                    timeIntervals={15}
                                    timeCaption='time'
                                    dateFormat='MMMM d, yyyy h:mm aa'
                                    selectsEnd
                                    startDate={dateFrom}
                                    endDate={dateTo}
                        />
                    </div>

                    <HorizontalDivider/>

                    <div className={SearchControlsStyles.search_string_input}>
                        <InputElement onSelectHandler={setSearchString}
                                      onChangeHandler={this.searchStringChangeHandler}
                                      // onAutocompleteSelectHandler={this.onAutocompleteSelectHandler}
                                      placeholder={i18next.t('search-input-text')}
                        />
                    </div>

                    <div onClick={this.onSearchClickHandler} id={'searchButton'}>
                        <SearchIcon className={SearchControlsStyles.search_button + searchBtnClassName}/>
                    </div>

                    {/*<Link className={SearchControlsStyles.about_field} to={'/about'} aria-label={'about'}>*/}
                    {/*<InfoIcon className={SearchControlsStyles.icon}/>*/}
                    {/*</Link>*/}
                </div>
            </React.Fragment>
        );
    }
}

export default SearchControls;
