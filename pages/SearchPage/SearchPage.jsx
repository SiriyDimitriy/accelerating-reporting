import React from 'react';
import Alert from 'react-s-alert';
import SearchControls from './SearchControls/SearchControls';
import {connect} from 'react-redux';
import {getInitialData} from '../../actions/commonActions';
// import {Redirect, Route, Switch} from 'react-router';
import {createStructuredSelector} from 'reselect';
// import {makeEntitiesCount} from '../../selectors/entities/entitiesSelectors';
// import {
//     makeSelectActiveDynamicField,
//     makeSelectDocTypes,
//     makeSelectSelectedDocType,
//     makeSelectTableView, makeShowLoaderData,
//     makeFacetsList, makeFacetsVisibility,
// } from '../../selectors/entities/commonSelectors';
import GlobalLoader from '../../components/simple/Loader/GlobalLoader';
import {makeSelectEntities, makeSelectItemsCount, makeShowLoaderData} from '../../selectors/commonSelectors';
import EntitiesListSection from './EntitiesListSection/EntitiesListSection';

const mapDispatchToProps = dispatch => ({
    getInitialData: () => dispatch(getInitialData()),
});

const mapStateToProps = createStructuredSelector({
    entities: makeSelectEntities(),
    showLoader: makeShowLoaderData(),
    itemsCount: makeSelectItemsCount(),
});

@connect(mapStateToProps, mapDispatchToProps)
class SearchPage extends React.Component {
    constructor(props) {
        super(props);

        this.onEnterHandler = this.onEnterHandler.bind(this);
    }

    onEnterHandler(event) {
        const {activeDynamicField, setDynamicField} = this.props;

        if (event.keyCode === 13) { // Number 13 is the "Enter" key on the keyboard
            event.preventDefault(); // Cancel the default action

            if (activeDynamicField) {
                if (activeDynamicField.interval) {
                    if (activeDynamicField.max || activeDynamicField.min) {
                        setDynamicField();
                    } else {
                        Alert.error('Please, set max or min value for selected field');
                    }
                } else {
                    setDynamicField();
                }
            } else {
                document.getElementById('searchButton').click();// Trigger the search button
            }
        }
    }

    componentDidMount() {
        const {getInitialData} = this.props;
        getInitialData();

        document.addEventListener('keyup', (event) => this.onEnterHandler(event));
    }

    render() {
        const {showLoader, itemsCount, entities} = this.props;

        return (
            <div>
                {showLoader && <GlobalLoader radius={'120px'} dotsCount={18} color={'#777'} dotSize={'13px'}/>}

                <SearchControls/>

                <EntitiesListSection itemsCount={itemsCount} entities={entities}/>

            </div>
        );
    }
}

export default SearchPage;
