import React from 'react';
import connect from 'react-redux/es/connect/connect';
import {withRouter} from 'react-router';
import {createStructuredSelector} from 'reselect';
import DocumentPageStyles from './EntitiesListSection.less';
import DndTable from '../../../components/coreComponents/DnDTable';
import {
    makeSelectPageNumber,
    makeSelectPageSize,
    // makeSelectEntities
} from '../../../selectors/commonSelectors';
import {withPagination} from '../../../components/coreComponents/decorators/withPagination/withPagination';
// import {
//     // deleteColumn,
//     // getTableData,
//     // reorderColumn,
//     // setPageNumber,
//     // setPageSize
// } from '../../../actions/commonActions';
// import {setSorting} from '../../actions/entities/searchActions';
import NoDataBlock from '../../../components/coreComponents/DnDTable/NoDataBlock/NoDataBlock';
import ExcelImport from '../../../components/coreComponents/decorators/withExcelImport/withExcelImport';
// import {isMsie} from '../../utils/navigatorUtils';

// const mapDispatchToProps = dispatch => ({
    // setDisplayedEntitiesNumber: (pageSize) => dispatch(setPageSize(pageSize)),
    // handleChangePage: (page) => dispatch(setPageNumber(page)),
    // handleDeleteColumn: (columnId, dataType) => dispatch(deleteColumn(columnId, dataType)),
    // handleReorderColumn: (startIndex, destinationIndex, dataType) => dispatch(reorderColumn(startIndex, destinationIndex, dataType)),
    // setSorting: (field) => dispatch(setSorting(field)),
    // getTableData: (docType) => dispatch(getTableData(docType)),
// });

const mapStateToProps = createStructuredSelector({
    itemsOnPage: makeSelectPageSize(),
    currentPage: makeSelectPageNumber(),
});

@withRouter
@connect(mapStateToProps)
@withPagination({
    pagesPagination: true,
    displayedEntriesPagination: true,
})
class EntitiesListSection extends React.PureComponent {
    constructor(props) {
        super(props);
        // this.state = {
        //     tableHeight: null,
        // };
    }

    // componentDidMount() {
    //     const {location: {pathname}, getTableData} = this.props;
    //     const dataType = pathname.substring(1);
    //
    //     getTableData(dataType);
    // }

    // componentDidUpdate() {
    //     this.calculateTableHeight();
    //     window.onresize = this.calculateTableHeight;
    // }

    onDeleteColumn = (columnId) => {
        const {location: {pathname}, handleDeleteColumn} = this.props;
        const dataType = pathname.substring(1);

        handleDeleteColumn(columnId, dataType);
    };

    // onDragEnd = (result) => {
    //     const {location: {pathname}, handleReorderColumn} = this.props;
    //     const dataType = pathname.substring(1);
    //
    //     handleReorderColumn(result.source.index, result.destination.index, dataType);
    // };

    // calculateTableHeight = () => {
    //     const footerPosition = document.getElementById('footer');
    //     const tableContent = document.getElementById('table_content');
    //
    //     if (tableContent && tableContent.offsetTop && footerPosition) {
    //
    //         let height = (footerPosition.offsetTop - tableContent.offsetTop);
    //         this.setState({
    //             tableHeight: height,
    //         });
    //     }
    // };

    render() {
        const {entities, setSorting} = this.props;

        // const {tableHeight} = this.state;
        // let tableStyle = {
        //     // height: `${tableHeight-30}px`,
        //     height: isMsie() ? `${tableHeight - 25}px` : `${tableHeight}px`,
        // };

        if (!entities) {
            return null;
        }

        if (!entities.entities) {
            return <NoDataBlock/>;
        }

        return (
            <div id='table_content'
                 className={DocumentPageStyles.content}
                 // style={tableStyle}
            >
                <React.Fragment>
                    <ExcelImport columns={entities && entities.columns}
                                 data={entities && entities.entities}
                                 filename={'SearchResults'}
                    />
                    <DndTable enumeratedRows={true}
                              {...entities}
                              handleSorting={setSorting}
                              handleDeleteColumn={this.onDeleteColumn}
                              onDragEnd={this.onDragEnd}
                              resizable={true}
                    />
                </React.Fragment>
            </div>
        );
    }
}

export default EntitiesListSection;
