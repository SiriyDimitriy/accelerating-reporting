import React from 'react';
import { connect } from 'react-redux';
import {
    Trans,
    // withNamespaces
} from 'react-i18next';

const mapStateToProps = state => {
    return {
        auth: state.auth,
    };
};

@connect(mapStateToProps)
// @withNamespaces(['errors'], {wait: true})
export default class ErrorPage extends React.Component {

    render() {
        const { errorCode, errorText } = this.props;
        return (
            <div className='error_page flex-column'>
                <div className='error_page_code'>
                    {errorCode || '404'} <Trans i18nKey='error'/>
                </div>
                <div className='error_page_message'>
                    {errorText || <Trans i18nKey='page_not_found'/>}
                </div>
            </div>
        );
    }
}