const webpack = require('webpack');
const {resolve} = require('path');
const config = require('./webpack.common.config.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const path = require('path');
const StyleLintPlugin = require('stylelint-webpack-plugin');
// const restPrefix = process.env.SEARCHENGINE_API_URL;

config.devtool = 'source-map';

config.mode = 'development';

config.entry = [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000/', // WebpackDevServer host and port
    'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
    './app.jsx',
];

config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({
        template: './htmlTemplates/SearchEngine.html',
        filename: resolve(__dirname, '../index.html'),
    }),
    new StyleLintPlugin({
        configFile: './.stylelintrc',
        // context: 'src',
        files: '**/*.less',
        failOnError: false,
        quiet: false,
    }),
);

config.devServer = {
    hot: true,
    historyApiFallback: true,
    inline: true,
    disableHostCheck: true,
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
    },
    publicPath: '/out/',

    // proxy: {
    //     [`/${restPrefix}`]: {
    //         target: 'http://10.100.101.107:9081',
    //         // pathRewrite: {'^/searchengine' : '/searchengine-api'}
    //     }
    //     // '/searchengine': 'http://localhost:8082',
    //     // '/': 'http://localhost:4005',
    //
    //     // logLevel: 'debug',
    //     // '/tree-microservice': {
    //     //   target: 'http://beta.idl.paradine.at:9080',
    //     // },
    //     // '/seam': {
    //     //   target: 'http://192.168.14.52:9083/portal',
    //     // },
    //     // [appBase]: {
    //     //   target: 'http://192.168.14.52:9083', // Nikulin local machine
    //     //   bypass: function(req, res, proxyOptions) {
    //     //     if (req.path.includes(`${appBase}/react-apps`)) {
    //     //       return req.path.replace(`${appBase}/react-apps`, '/out');
    //     //     }
    //     //   },
    //     // },
    // },
    // before: function(app) {
    //     app.use(apiMocker('/api', 'mocks/api'));
    // },
};

module.exports = config;
