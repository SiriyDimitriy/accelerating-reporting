import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware, {END} from 'redux-saga';
import {createLogger} from 'redux-logger';
import RootReducer from './reducers/RootReducer';
import rootSaga from './sagas/RootSaga';

const loggerMiddleware = createLogger();
const sagaMiddleware = createSagaMiddleware();

function configureStore(initialState) {
    let middleware = [sagaMiddleware];

    if (process.env.NODE_ENV !== 'production') {
        middleware = [sagaMiddleware, loggerMiddleware];
    }

    return createStore(
        RootReducer,
        initialState,
        applyMiddleware(...middleware)
    );
}

let store = configureStore();

store.runSaga = sagaMiddleware.run;
store.close = () => store.dispatch(END);

store.runSaga(rootSaga);

export default store;