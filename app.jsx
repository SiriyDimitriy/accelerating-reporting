import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {HashRouter} from 'react-router-dom';
import {I18nextProvider} from 'react-i18next';
import i18n from './utils/i18n';
// import './styles/resetCSS.css';
import './styles/common.less';
// import 'url-search-params-polyfill'; //IE support of search params
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/scale.css';
// import 'react-datepicker/dist/react-datepicker.css';

// CSS Modules, react-datepicker-cssmodules.css
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

// if (!global._babelPolyfill) { //fix issue for the multiple instances
//     require('babel-polyfill');
// }

import store from './Store.js';
import Application from './components/Application';

const render = () => {
    ReactDOM.render(
        <I18nextProvider i18n={i18n}>
            <Provider store={store}>
                <HashRouter>
                    <Application/>
                </HashRouter>
            </Provider>
        </I18nextProvider>,
        document.getElementById('root'),
    );
};

window.onload = () => { //load styles before js-files interpretation with browser
    render();
};

if (module.hot) {
    module.hot.accept();
    window.onload = () => { //load styles before js-files interpretation with browser
        render();
    };
}