import {put, call, select} from 'redux-saga/effects';
import {createSearchUrl} from '../../utils/requestUrls/requestUrlUtils';
import {
    makeSelectDateFrom,
    makeSelectDateTo,
    makeSelectPageNumber,
    makeSelectPageSize,
    makeSelectUserString,
} from '../../selectors/commonSelectors';
import instance from '../../utils/httpService';

export function* getEntities() {
    const dateFrom = yield select(makeSelectDateFrom());
    const dateTo = yield select(makeSelectDateTo());
    const pageNumber = yield select(makeSelectPageNumber());
    const pageSize = yield select(makeSelectPageSize());
    const userString = yield select(makeSelectUserString());

    const requestUrl = createSearchUrl(dateFrom && dateFrom.toISOString(), dateTo && dateTo.toISOString(), pageSize, pageNumber, userString);

    let response;
    try {
        response = yield call(instance.doRequest, 'GET', requestUrl);
    } catch (e) {
        response = [];
    }

    yield put({type: 'GET_ITEMS_COUNT', count: response.meta && response.meta.results});

    // default 'must have' columns
    let columns = [
        {
            id: 'itemNum',
            label: '#',
            type: 'Integer',
            unit: null,
            isLink: false,
            sorting: false,
            isDragDisabled: true,
            isDeleteDisabled: true,
            isVisible: true
        },
    ];

    let itemNum = pageNumber * pageSize + 1;
    let entities = [];

    columns.push({
        id: 'account',
        label: 'Account',
        type: 'String',
    });

    columns.push({
        id: 'timestamp',
        label: 'Timestamp',
        type: 'String',
    });

    columns.push({
        id: 'requestService',
        label: 'Request Service',
        type: 'String',
    });

    columns.push({
        id: 'requestIRDI',
        label: 'Request IRDI',
        type: 'String',
    });

    columns.push({
        id: 'requestStatus',
        label: 'Request Status',
        type: 'String',
    });

    response && response.data && response.data.map(item => {
        let dataObj = {};

        Object.keys(item).map(key => {
            dataObj[key] = item[key];
        });

        dataObj.itemNum = itemNum++;

        entities.push(dataObj);
    });

    yield put({type: 'GET_ENTITIES', entities: {entities, columns}});
}

// export function* setPageSize(action) {
//     const {pageSize} = action;
//
//     yield put({type: 'SET_PAGE_SIZE', pageSize});
//     yield put({type: 'SET_PAGE_NUMBER', pageNumber: 0});
//     yield all([
//         call(getEntities),
//         call(getEntitiesCount)
//     ]);
// }
//
// export function* setPageNumber(action) {
//     const {pageNumber} = action;
//
//     yield put({type: 'SET_PAGE_NUMBER', pageNumber});
//     if (pageNumber !== 0) {
//         yield all([
//             call(getEntities),
//             call(getEntitiesCount)
//         ]);
//     }
// }

