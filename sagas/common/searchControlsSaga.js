// import {createDocumentUrl} from '../../utils/requestUrls/requestUrlUtils';
import {put} from 'redux-saga/effects';
// import {sagaMakeDefaultApi, sagaMakeSelectedAlternateApi} from '../../selectors/entities/apiSelectors';

export function* setDateFrom(action) {
    yield put({type: 'UPDATE_DATE_FROM', date: action.date});
}

export function* setDateTo(action) {
    yield put({type: 'UPDATE_DATE_TO', date: action.date});
}

export function* setUserString(action) {
    yield put({type: 'UPDATE_USER_STRING', string: action.string});
}
