import {put} from 'redux-saga/effects';

export function* getGUILanguagesList() {
    let lngs = ['en_US', 'de_DE'];
    // try {
    //     lngs = yield call(instance.doRequest, 'GET', requestUrl);
    // } catch (e) {
    //     lngs = [];
    // }

    let supportedGUILanguages = [];
    if (lngs && lngs.length !==0) {
        supportedGUILanguages = lngs.map(el => {
            return {
                // label: el,
                value: el,
                key: `language_${el.split('_')[0]}`
            };
        });
    }

    yield put({type: 'GET_GUI_LANGUAGES_LIST', lngs: supportedGUILanguages});
}
