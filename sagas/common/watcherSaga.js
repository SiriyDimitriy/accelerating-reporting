import {
    GET_ENTITIES,
    GET_GUI_LANGUAGE_LIST, GET_INITIAL_DATA, SET_DATE_FROM, SET_DATE_TO, SET_USER_STRING,
} from '../../constants/actionTypes/commonConstants';
import {takeLatest, put} from 'redux-saga/effects';
import {getGUILanguagesList} from './GUILanguageSaga';
import {toggleLoader} from '../../actions/commonActions';
import {setDateFrom, setDateTo, setUserString} from './searchControlsSaga';
import {getEntities} from './entitiesSaga';

function* getInitialData() {
    yield getGUILanguagesList();
}

function* useLoader(saga, action) {
    try {
        yield put(toggleLoader(true));
        if (action) {
            yield saga(action);
        }
        yield put(toggleLoader(false));
    } catch (e) {
        yield put(toggleLoader(false));
    }
}

export function* watcherSaga() {
    yield takeLatest(GET_INITIAL_DATA, useLoader, getInitialData);
    yield takeLatest(GET_GUI_LANGUAGE_LIST, getGUILanguagesList);
    yield takeLatest(SET_DATE_FROM, setDateFrom);
    yield takeLatest(SET_DATE_TO, setDateTo);
    yield takeLatest(SET_USER_STRING, setUserString);
    yield takeLatest(GET_ENTITIES, getEntities);
}
