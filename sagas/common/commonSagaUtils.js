import { call } from 'redux-saga/effects';
import request from '../../utils/httpService';
import { setCookie } from '../../utils/cookiesUtils';
import { createGetTokenUrl } from '../../utils/requestUrls/commonRequestUrls';

export function* getToken() {
    const requestURL = createGetTokenUrl();
    const token = yield call(request.doRequest, 'GET', requestURL);

    yield call(setCookie, 'Authorization', `Bearer ${token.jwtToken}`, 1000);

    return token;
}
