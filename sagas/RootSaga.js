import { all } from 'redux-saga/effects';
import { watcherSaga } from './common/watcherSaga';

let saga;

saga = watcherSaga;

export default function* rootSaga() {
    yield all([
        saga(),
    ]);
}
