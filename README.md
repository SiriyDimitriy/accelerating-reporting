
## Setup

1. Install [Node.js](https://nodejs.org/) (v6.0+)
2. Go to project folder and run `npm install`

For development
4. `npm run dev`
5. Go to `http://localhost:4005`

For production
4. `npm run build_app` if you need to get js-file in '/out', also need to define REPORTING_API_URL somewhere, like for dev script

## Configs

* Client hot module reloading
* Custom Babel presets with ES6 support, optional React support and optimizations for polyfilling Node and browser builds.
* CSS Modules and LESS support
* Static asset support
* Inline SVG support
* Style and script linter rulesets

## Command line

There is few possible options to run code, both set as scripts in package.json:

- for development purposes:
```
npm run dev
```
- for production:

```
npm run build_app
```

Here are all available commands:

* [`dev`]() starts a common development environment
* [`build_app`]() compiles server and client code for production use
