import {createSelector} from 'reselect';

const selectLoader = state => state && state.loader;
const selectGUILanguages = state => state && state.GUILanguages;
const selectSearchControls = state => state && state.searchControls;
const selectSearchResults = state => state && state.searchResults;
const selectItemsCount = state => state && state.count;

const makeSelectGUILanguages = () => createSelector(selectGUILanguages, data => data && data.GUILanguages);
const makeShowLoaderData = () => createSelector(selectLoader, data => data && data.showLoader);
const makeSelectDateFrom = () => createSelector(selectSearchControls, data => data && data.dateFrom);
const makeSelectDateTo = () => createSelector(selectSearchControls, data => data && data.dateTo);
const makeSelectUserString = () => createSelector(selectSearchControls, data => data && data.userString);
const makeSelectPageSize = () => createSelector(selectSearchResults, data => data && data.pageSize);
const makeSelectPageNumber = () => createSelector(selectSearchResults, data => data && data.pageNumber);
const makeSelectEntities = () => createSelector(selectSearchResults, data => data && data.entities);
const makeSelectItemsCount = () => createSelector(selectItemsCount, data => data && data.count);

export {
    makeShowLoaderData,
    makeSelectGUILanguages,
    makeSelectDateFrom,
    makeSelectDateTo,
    makeSelectUserString,
    makeSelectPageSize,
    makeSelectPageNumber,
    makeSelectEntities,
    makeSelectItemsCount,
};
