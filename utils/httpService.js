// import 'whatwg-fetch';// Fetch polyfill for IE, Edge
import Alert from 'react-s-alert';
import {errMessages} from './errMessages';

class HttpError extends Error {
    constructor(response) {
        super();
        this.response = response;
    }
}

class HttpService {
    constructor() {
        this.handleError = this.handleError.bind(this);
        this.doRequest = this.doRequest.bind(this);
    }

    setHistory(history) {
        this.history = history;
    }

    doRequest(method, route, requestData, transformResponse, errorFunction) {
        let options = {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjM2MTU2NTM1MTM5MywidXNlcklkIjoxLCJ1c2VyTmFtZSI6IkFkbWluaXN0cmF0b3IifQ.54QOzidOS-HXP9BGGpxt3nG5bkTY78Vd-UloRygMQ9w',
            },
        };

        if (requestData) {
            options.body = JSON.stringify(requestData);
        }

        return fetch(route, options)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }

                throw new HttpError(response);
            })
            .then(res => {
                if (transformResponse) {
                    return transformResponse(res);
                }

                return res;
            })
            .catch(e => {
                this.handleError(e, {route: route, method: method}, errorFunction);
            });
    }

    handleError(e, errorObj, errorFunction) {
        console.error(e);

        if (errorFunction) {
            errorFunction(e);
            return;
        }

        if (!e.response) {
            Alert.error(`${e}, \
            Request url: ${errorObj.route}, \
            Method: ${errorObj.method}`, {});
        }

        const messKey = Object.keys(errMessages).find(key => key === e.response.status.toString());

        Alert.error(errMessages[messKey] || e.response.statusText, {});
    }
}

let instance = new HttpService();

const initHttpServer = (history) => {
    instance.setHistory(history);
};

export default instance;

export {initHttpServer};
