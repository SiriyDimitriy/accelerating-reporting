export function deepCopy(object) {
    return JSON.parse(JSON.stringify(object));
}

export function stringifyData(object) { //for react module in eptos, because of overriden toJSON method
    const oldToJSON = [].toJSON;

    Object.extend(Array.prototype, { toJSON: null });
    const result = JSON.stringify(object);
    Object.extend(Array.prototype, { toJSON: oldToJSON });

    return result;
}
