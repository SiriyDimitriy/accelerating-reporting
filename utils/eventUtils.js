export function createCustomEvent(eventName, details) { // IE11 support
    let event;

    if (typeof window.CustomEvent === 'function') {
        event = new CustomEvent(eventName, {detail: details});
    } else {
        event = document.createEvent('CustomEvent');
        event.initCustomEvent(eventName, true, true, details);
    }

    return event;
}
