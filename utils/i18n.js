import i18next from 'i18next';
import XHR from 'i18next-xhr-backend';
import {initReactI18next} from 'react-i18next';
import {getCookie} from './cookiesUtils';

const isDev = process.env.NODE_ENV === 'development';

i18next
    .use(XHR)
    .use(initReactI18next)
    .init({
        backend: {
            crossDomain: false, // allow cross domain requests

            withCredentials: false, // allow credentials on cross domain requests

            loadPath: '/locales/{{lng}}/messages.json',
        },

        debug: isDev,
        wait: false,

        // have a common namespace used around the full app
        // ns: ['alerts', 'errors', 'navigation', 'settings'],
        // defaultNS: 'alerts',
        // fallbackNS: true,

        interpolation: {
            escapeValue: false, // not needed for react!!
            // formatSeparator: ',',
        },

        react: {
            wait: false,
            useSuspense: false,
        },

        lng: getCookie('locale') || 'en_US',
        // fallbackLng: false, // use en_US if detected lng is not available
        // keySeparator: false, // we do not use keys in form messages.welcome

    });

export default i18next;
