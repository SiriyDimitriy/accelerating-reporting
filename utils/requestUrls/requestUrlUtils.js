const baseApiUrl = process.env.REPORTING_API_URL;

const createCountUrl = (api) => {
    return `${api}/documents/count`;
};

const createGUILanguagesUrl = () => {
    return `${baseApiUrl}/configurations/guiLocale`;
};

const createSearchUrl = (dateFrom, dateTo, limit, offset, userString) => {
    const searchString = new URLSearchParams();
    if (dateFrom) {
        searchString.append('fromTimestamp', dateFrom);
    }

    if (dateTo) {
        searchString.append('toTimestamp', dateTo);
    }

    if (limit) {
        searchString.append('limit', limit);
    }

    if (offset) {
        searchString.append('limit', offset);
    }

    if (userString) {
        searchString.append('user', userString);
    }

    return `${baseApiUrl}/api/log-dashboard-api/?${searchString.toString()}`;
};

export {
    createCountUrl,
    createGUILanguagesUrl,
    createSearchUrl,
};

