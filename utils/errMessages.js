const errMessages = {
    401: 'you_are_not_authorized',
    403: 'requested_resource_forbidden',
    404: 'requested_resource_not_found',
    400: 'bad_request',
    500: 'internal_server_error',
    503: 'server_temporarily_unavailable'
};

export {
    errMessages
};