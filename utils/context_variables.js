let moduleName = null;
let entitiesTableName = null;
let history = null;

export const setModuleName = (name) => {
    moduleName = name;
};

export const getModuleName = () => moduleName;

export const setEntitiesTableName = (name) => {
    entitiesTableName = name;
};

export const getEntitiesTableName = () => entitiesTableName;

export const setHistory = (h) => {
    history = h;
};

export const getHistory = () => history;


let trans = null;

export const setTrans = (t) => {
    trans = t;
};

export const getTrans = () => trans;
